function kiemTraRong(value, idErr) {
  if (value == "") {
    document.getElementById(idErr).innerHTML = "Điền đi mài!!!!"
    return false
  } else {
    document.getElementById(idErr).innerHTML = ""
    return true
  }
}

function kiemTraTrung(value, idErr, ds, message, key) {
  var index = ds.findIndex((a) => {
    return a[key] == value
  })
  if (index !== -1) {
    document.getElementById(idErr).innerHTML = message
    return false
  } else {
    document.getElementById(idErr).innerHTML = ""
    return true
  }
}
