var dssv = []
const DSSV_LOCAL = "DSSV"

// render lại data từ localStorage khi user reload
var dataJson = localStorage.getItem(DSSV_LOCAL)

if (dataJson != null && dataJson.trim() !== "") {
  dssv = JSON.parse(dataJson).map(function (sv) {
    return new SinhVien(
      sv.ma,
      sv.ten,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    )
  })
  renderDSSV(dssv)
}

function themSV() {
  var sv = layThongTinTuForm()

  // validate dữ liệu
  var isValid =
    kiemTraRong(sv.ma, "spanMaSV") &
    kiemTraRong(sv.ten, "spanTenSV") &
    kiemTraRong(sv.matKhau, "spanMatKhau") &
    kiemTraRong(sv.email, "spanEmailSV")
  if (!isValid) {
    return
  }

  var isMaSVUnique = kiemTraTrung(
    sv.ma,
    "spanMaSV",
    dssv,
    "Mã sinh viên đã tồn tại!",
    "ma"
  )
  var isEmailUnique = kiemTraTrung(
    sv.email,
    "spanEmailSV",
    dssv,
    "Email đã tồn tại!",
    "email"
  )

  if (!isMaSVUnique || !isEmailUnique) {
    return
  }

  dssv.push(sv)
  console.log("🏎 ~ themSV ~ dssv:", dssv)
  document.getElementById("notification").innerHTML =
    "Thêm thành công! Bấm nút Reset để thêm sinh viên mới nhé!"

  renderDSSV(dssv)

  // convert object to json
  var dataJson = JSON.stringify(dssv)
  // lưu xuống local storage
  localStorage.setItem("DSSV", dataJson)
}

function xoaSV(id) {
  console.log("🏎 ~ xoaSV ~ id:", id)

  // tìm vị trí của sv cần xoá có mã = id
  var index
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i]
    if (sv.ma == id) {
      index = i
    }
  }
  // xoá sv khỏi mảng
  dssv.splice(index, 1)
  // render lại mảng
  renderDSSV(dssv)
}

function suaSV(id) {
  console.log("🏎 ~ suaSV ~ id:", id)
  var index = dssv.findIndex((a) => {
    return a.ma == id
  })
  var sv = dssv[index]
  document.getElementById("notification").innerHTML =
    "Thay đổi giá trị và bấm nút Cập Nhật để lưu lại nhé!"
  document.getElementById("txtMaSV").value = sv.ma
  document.getElementById("txtMaSV").readOnly = true
  document.getElementById("spanMaSV2").innerHTML =
    "Bạn không thể sửa mã SV nhé!"
  document.getElementById("txtTenSV").value = sv.ten
  document.getElementById("txtPass").value = sv.matKhau
  document.getElementById("txtEmail").value = sv.email
  document.getElementById("txtDiemToan").value = sv.toan
  document.getElementById("txtDiemLy").value = sv.ly
  document.getElementById("txtDiemHoa").value = sv.hoa
  console.log("🏎 ~ suaSV ~ sv:", sv)
}

function resetInput() {
  document.getElementById("notification").innerHTML = ""
  document.getElementById("txtMaSV").value = ""
  document.getElementById("txtMaSV").readOnly = false
  document.getElementById("spanMaSV").innerHTML = ""
  document.getElementById("spanMaSV2").innerHTML = ""
  document.getElementById("txtTenSV").value = ""
  document.getElementById("spanTenSV").innerHTML = ""
  document.getElementById("txtPass").value = ""
  document.getElementById("spanMatKhau").innerHTML = ""
  document.getElementById("txtEmail").value = ""
  document.getElementById("spanEmailSV").innerHTML = ""
  document.getElementById("txtDiemToan").value = ""
  document.getElementById("txtDiemLy").value = ""
  document.getElementById("txtDiemHoa").value = ""
}

function capNhatSV() {
  console.log("🏎 ~ capNhatSV ~ dssv:", dssv)
  var maSV = document.getElementById("txtMaSV").value
  var tenSV = document.getElementById("txtTenSV").value
  var matKhau = document.getElementById("txtPass").value
  var email = document.getElementById("txtEmail").value
  var diemToan = parseFloat(document.getElementById("txtDiemToan").value)
  var diemLy = parseFloat(document.getElementById("txtDiemLy").value)
  var diemHoa = parseFloat(document.getElementById("txtDiemHoa").value)

  // Tìm vị trí của sinh viên
  var index = dssv.findIndex((a) => {
    return a.ma == maSV
  })

  // Nếu tìm thấy sinh viên, cập nhật thông tin của nó
  if (index !== -1) {
    dssv[index].ten = tenSV
    dssv[index].matKhau = matKhau
    dssv[index].email = email
    dssv[index].toan = diemToan
    dssv[index].ly = diemLy
    dssv[index].hoa = diemHoa
    dssv[index].tinhDTB = function () {
      return (this.toan + this.ly + this.hoa) / 3
    }
  }
  renderDSSV(dssv)
  // Lưu xuống local storage
  var dataJson = JSON.stringify(dssv)
  localStorage.setItem("DSSV", dataJson)
}

function timSV_input() {
  // Add an event listener to the search input
  document.getElementById("txtSearch").addEventListener("input", function () {
    var keyword = this.value.trim() // Remove leading/trailing whitespace
    console.log("🏎 ~ timSV ~ keyword:", keyword)

    if (keyword === "") {
      // If the search input is empty, render the full list (dssv)
      renderDSSV(dssv)
      return
    }

    var mangTimKiem = dssv.filter(function (sv) {
      return sv.ten.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
    })

    console.log("🏎 ~ timSV ~ mangTimKiem:", mangTimKiem)
    renderDSSV(mangTimKiem)
  })
}

function timSV() {
  var keyword = document.getElementById("txtSearch").value
  console.log("🏎 ~ timSV ~ keyword:", keyword)

  var mangTimKiem = dssv.filter(function (sv) {
    return sv.ten.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
  })
  console.log("🏎 ~ timSV ~ mangTimKiem:", mangTimKiem)
  renderDSSV(mangTimKiem)
}
